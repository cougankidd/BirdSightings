import { useGetAllFeaturedBirdsQuery } from "./redux/apiSlice";
import { Carousel } from "react-bootstrap";
import "./css/MainPage.css";

function MainPage() {
  const { data } = useGetAllFeaturedBirdsQuery();

  return (
    <>
      <div className="bg-layer"></div>
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold white-text">Bitterroot Birds</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4 white-text">
            Bitterroot Birds is your ultimate online guide to exploring the
            diverse and vibrant birdlife of the Bitterroot Valley. Whether
            you're a seasoned birder or a curious novice, our website provides a
            wealth of information, resources, and a sense of community to
            enhance your birding adventures.
          </p>
        </div>
        <Carousel fade>
          {data?.Birds.map((bird) => {
            return (
              <Carousel.Item key={bird.id} interval={6000}>
                <img
                  className="carousel w-100 d-block"
                  src={bird.picture_url}
                  alt="..."
                />
                <Carousel.Caption>
                  <h3> {bird.name} </h3>
                  <p> {bird.description} </p>
                </Carousel.Caption>
              </Carousel.Item>
            );
          })}
        </Carousel>
      </div>
    </>
  );
}

export default MainPage;
