import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { birdApi } from "./apiSlice";

export const store = configureStore({
  reducer: {
    [birdApi.reducerPath]: birdApi.reducer,
  },

  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(birdApi.middleware),
});

setupListeners(store.dispatch);
