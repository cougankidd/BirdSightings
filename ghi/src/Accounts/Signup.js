import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useSignupMutation } from "../redux/apiSlice";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBInput,
} from "mdb-react-ui-kit";

export default function Signup() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  const [signup] = useSignupMutation();
  const [usernameExists, setUsernameExists] = useState(false);

  async function handleSubmit(e) {
    e.preventDefault();

    const response = await signup({
      username,
      password,
    });
    if (response.data) {
      navigate("/");
    } else {
      setUsernameExists(true);
    }
  }
  return (
    <>
      <div className="bg-layer"></div>
      <MDBContainer className="signup">
        <MDBRow>
          <MDBCol></MDBCol>
          <MDBCol>
            <div className="card text-bg-light mb-3">
              <h5 className="card-header">Signup</h5>
              <div className="card-body">
                <form onSubmit={(e) => handleSubmit(e)}>
                  <div className="mb-3">
                    <label className="form-label" id="form-username2">
                      Username
                    </label>
                    <MDBInput
                      name="text"
                      type="text"
                      className="form-input"
                      onChange={(e) => setUsername(e.target.value)}
                    />
                  </div>
                  <div className="mb-3">
                    <label className="form-label" id="form-password2">
                      Password
                    </label>
                    <MDBInput
                      name="password"
                      type="password"
                      className="form-input"
                      onChange={(e) => setPassword(e.target.value)}
                    />
                  </div>
                  {usernameExists && (
                    <div className="mb-3">
                      <span style={{ color: "red" }}>
                        Username already exists. Please choose a different
                        username.
                      </span>
                    </div>
                  )}
                  <MDBBtn outline rounded type="submit" color="info" block>
                    Sign up
                  </MDBBtn>
                </form>
              </div>
            </div>
          </MDBCol>
          <MDBCol></MDBCol>
        </MDBRow>
      </MDBContainer>
    </>
  );
}
