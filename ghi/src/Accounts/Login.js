import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useLoginMutation } from "../redux/apiSlice";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBInput,
} from "mdb-react-ui-kit";
import "../css/Accounts.css";

export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  const [login] = useLoginMutation();
  const [loginError, setLoginError] = useState(false);

  async function handleSubmit(e) {
    e.preventDefault();

    const response = await login({
      username,
      password,
    });
    if (response.data) {
      navigate("/");
    } else {
      setLoginError(true);
    }
  }
  return (
    <>
      <div className="bg-layer"></div>
      <MDBContainer className="login">
        <MDBRow>
          <MDBCol></MDBCol>
          <MDBCol>
            <div className="card text-bg-light mb-3">
              <h5 className="card-header">Login</h5>
              <div className="card-body">
                <form onSubmit={(e) => handleSubmit(e)}>
                  <div className="mb-3">
                    <label className="form-label" id="form-username">
                      Username
                    </label>
                    <MDBInput
                      name="text"
                      type="text"
                      className="form-input"
                      onChange={(e) => setUsername(e.target.value)}
                    />
                  </div>
                  <div className="mb-3">
                    <label className="form-label" id="form-password">
                      Password
                    </label>
                    <MDBInput
                      name="password"
                      type="password"
                      className="form-input"
                      onChange={(e) => setPassword(e.target.value)}
                    />
                  </div>
                  {loginError && (
                    <div className="mb-3">
                      <span style={{ color: "red" }}>
                        Invalid username or password. Please try again.
                      </span>
                    </div>
                  )}
                  <MDBBtn outline rounded type="submit" color="info" block>
                    Login
                  </MDBBtn>
                </form>
              </div>
            </div>
          </MDBCol>
          <MDBCol></MDBCol>
        </MDBRow>
      </MDBContainer>
    </>
  );
}
