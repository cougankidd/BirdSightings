import { useState } from "react";
import { useGetAllBirdSightingsQuery } from "../redux/apiSlice";

export default function Recentsightings() {
  const { data, isLoading } = useGetAllBirdSightingsQuery();
  const [searchQuery, setSearchQuery] = useState("");
  const filteredSightings = data?.bird_sightings.filter((bird) =>
    bird.name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  if (isLoading) {
    return <div>IsLoading...</div>;
  }

  const handleSearch = (e) => {
    setSearchQuery(e.target.value);
  };

  return (
    <>
      <div className="bg-layer"></div>
      <div className="Birds white-text">
        <h1>Recent Bird Sightings</h1>
        <div className="box">
          <input
            type="text"
            placeholder="Search by bird name..."
            value={searchQuery}
            onChange={handleSearch}
            style={{
              width: "300px",
              height: "40px",
              fontSize: "18px",
              marginBottom: "20px",
            }}
          />
        </div>
        <table className="table table-striped">
          <thead className="black-text">
            <tr>
              <th scope="col">Bird Sighted</th>
              <th scope="col">Date</th>
              <th scope="col">Notes</th>
              <th scope="col">Image</th>
            </tr>
          </thead>
          <tbody>
            {filteredSightings
              ?.sort((a, b) => new Date(b.date) - new Date(a.date))
              .map((bird) => (
                <tr key={bird.id}>
                  <td style={{ color: "black" }}>{bird.name}</td>
                  <td style={{ color: "black" }}>{bird.date2}</td>
                  <td style={{ color: "black" }}>{bird.notes}</td>
                  <td>
                    <img src={bird.picture_url} height="150" alt={bird.name} />
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    </>
  );
}
