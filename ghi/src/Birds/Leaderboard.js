import { useGetAllUsersQuery } from "../redux/apiSlice";
import "../css/Birds.css";

export default function Leaderboard() {
  const { data, isLoading } = useGetAllUsersQuery();

  if (isLoading) {
    return <div>IsLoading...</div>;
  }

  return (
    <>
      <div className="bg-layer"></div>
      <div className="Birds">
        <h1 className="white-text">Leaderboard</h1>
        <table className="table table-striped">
          <thead className="black-text">
            <tr>
              <th scope="col">Username</th>
              <th scope="col"># of Bird Sightings</th>
            </tr>
          </thead>
          {data?.accounts
            .slice()
            .sort((a, b) => b.bird_sightings - a.bird_sightings)
            .map((account) => {
              return (
                <tbody key={account.id}>
                  <tr>
                    <td style={{ color: "black" }}>{account.username}</td>
                    <td style={{ color: "black" }}>{account.bird_sightings}</td>
                  </tr>
                </tbody>
              );
            })}
        </table>
      </div>
    </>
  );
}
