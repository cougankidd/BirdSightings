import { useEditBirdSightingMutation } from "../redux/apiSlice";
import { useGetsingleBirdSightingQuery } from "../redux/apiSlice";
import { useGetAllBirdsQuery } from "../redux/apiSlice";
import { useState } from "react";
import { MDBTextArea } from "mdb-react-ui-kit";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import { useEffect } from "react";
import "../css/Birds.css";

export default function EditSighting() {
  const navigate = useNavigate();
  const [edit] = useEditBirdSightingMutation();
  const params = useParams();
  const { data: current, isLoading } = useGetsingleBirdSightingQuery(
    params.bird_id
  );
  const { data: birdnames, isLoading: birdnamesloading } =
    useGetAllBirdsQuery();
  const [name, setName] = useState("");
  const [date, setDate] = useState("");
  const [notes, setNotes] = useState("");

  useEffect(() => {
    if (current) {
      setDate(current.bird_sighting.date);
      setName(current.bird_sighting.name);
      setNotes(current.bird_sighting.notes);
    }
  }, [current]);

  if (isLoading || birdnamesloading) {
    return <div>IsLoading...</div>;
  }

  async function handleSubmit(e) {
    e.preventDefault();
    const submitdata = {
      name: name,
      date: date,
      notes: notes,
    };
    const bird_id = params.bird_id;
    edit({
      bird_id,
      ...submitdata,
    });

    navigate("/recentusersightings");
  }

  return (
    <>
      <div className="bg-layer"></div>
      <h1 className="white-text">Edit a Sighting</h1>
      <form onSubmit={(e) => handleSubmit(e)}>
        <div className="mb-3">
          <select
            onChange={(e) => setName(e.target.value)}
            value={name}
            required
            name="name"
            className="form-select"
          >
            <option value="">{name}</option>
            {birdnames.Birds?.map((bird) => {
              return (
                <option key={bird.id} value={bird.bird_name}>
                  {bird.bird_name}
                </option>
              );
            })}
          </select>
        </div>
        <div className="mb-3">
          <label htmlFor="date" className="form-label" id="date-color">
            Date
          </label>
          <input
            type="date"
            className="form-control"
            id="date"
            value={date}
            onChange={(e) => setDate(e.target.value)}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="notes" className="form-label" id="notes-color">
            Notes
          </label>
          <MDBTextArea
            className="form-control"
            value={notes}
            id="notes"
            required
            onChange={(e) => setNotes(e.target.value)}
            style={{ backgroundColor: "white" }}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Change
        </button>
      </form>
    </>
  );
}
