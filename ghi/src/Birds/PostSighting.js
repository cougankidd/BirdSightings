import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  usePostSightingMutation,
  useGetAllBirdsQuery,
} from "../redux/apiSlice";
import { MDBTextArea } from "mdb-react-ui-kit";
import "../css/Birds.css";

export default function PostSighting() {
  const [name, setName] = useState("");
  const [date, setDate] = useState("");
  const [notes, setNotes] = useState("");
  const navigate = useNavigate();
  const [postSighting] = usePostSightingMutation();
  const { data, isLoading } = useGetAllBirdsQuery();

  if (isLoading) {
    return <div>IsLoading...</div>;
  }

  async function handleSubmit(e) {
    e.preventDefault();
    const response = await postSighting({
      name,
      date,
      notes,
    });
    if (response.data) {
      navigate("/recentusersightings");
    }
  }

  return (
    <>
      <div className="bg-layer"></div>
      <h1 className="white-text">Post a Sighting</h1>
      <form onSubmit={(e) => handleSubmit(e)} className="form-container">
        <div className="mb-3">
          <select
            onChange={(e) => setName(e.target.value)}
            value={name}
            required
            name="name"
            className="form-select"
          >
            <option value="">Select a Bird</option>
            {data.Birds?.map((bird) => {
              return (
                <option key={bird.id} value={bird.bird_name}>
                  {bird.bird_name}
                </option>
              );
            })}
          </select>
        </div>
        <div className="mb-3">
          <label htmlFor="date" className="form-label" id="date-color2">
            Date
          </label>
          <input
            type="date"
            className="form-control"
            id="date"
            onChange={(e) => setDate(e.target.value)}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="notes" className="form-label" id="notes-color2">
            Notes
          </label>
          <MDBTextArea
            className="form-control"
            id="notes"
            required
            onChange={(e) => setNotes(e.target.value)}
            style={{ backgroundColor: "white" }}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Report
        </button>
      </form>
    </>
  );
}
