import React, { useState } from "react";
import { useGetAllBirdsQuery } from "../redux/apiSlice";
import {
  MDBCard,
  MDBCardImage,
  MDBCardTitle,
  MDBRow,
  MDBCol,
} from "mdb-react-ui-kit";
import "../css/Birds.css";

export default function Birds() {
  const { data, isLoading } = useGetAllBirdsQuery();
  const [searchQuery, setSearchQuery] = useState("");

  if (isLoading) {
    return <div>IsLoading...</div>;
  }

  const filteredBirds = data?.Birds.filter((bird) =>
    bird.bird_name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  return (
    <>
      <div className="bg-layer"></div>
      <div className="Birds white-text">
        <h1>Birds</h1>
        <div className="box">
          <input
            type="text"
            placeholder="Find a Bird!"
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
            style={{
              width: "300px",
              height: "40px",
              fontSize: "18px",
              marginBottom: "20px",
            }}
          />
        </div>
        <MDBRow className="row-cols-1 row-cols-md-5 g-4">
          {filteredBirds?.map((bird) => {
            return (
              <MDBCol key={bird.id}>
                <MDBCard
                  className="h-100"
                  style={{ boxShadow: "0 4px 8px rgba(0, 0, 0, 0.4)" }}
                >
                  <MDBCardTitle className="text-center black-text">
                    {bird.bird_name}
                  </MDBCardTitle>
                  <MDBCardImage position="bottom" src={bird.picture_url} />
                </MDBCard>
              </MDBCol>
            );
          })}
        </MDBRow>
      </div>
    </>
  );
}
