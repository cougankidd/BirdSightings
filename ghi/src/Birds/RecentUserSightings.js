import { useGetAllUserBirdSightingsQuery } from "../redux/apiSlice";
import { useDeleteUserBirdSightingsMutation } from "../redux/apiSlice";
import { useNavigate } from "react-router-dom";
import { useState } from "react";

export default function RecentUserSightings() {
  const { data, isLoading } = useGetAllUserBirdSightingsQuery();
  const [DeleteUserBirdSightings] = useDeleteUserBirdSightingsMutation();
  const navigate = useNavigate();
  const [searchQuery, setSearchQuery] = useState("");

  if (isLoading) {
    return <div>IsLoading...</div>;
  }

  const handleDelete = (e) => {
    DeleteUserBirdSightings(e.target.value);
  };
  const handleEdit = (e) => {
    navigate(`/editsighting/${e.target.value}`);
  };

  const filteredSightings = data?.bird_sightings.filter((bird) =>
    bird.name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const handleSearch = (e) => {
    setSearchQuery(e.target.value);
  };

  const sortedSightings = filteredSightings?.sort(
    (a, b) => new Date(b.date) - new Date(a.date)
  );

  return (
    <>
      <div className="bg-layer"></div>
      <div className="Birds white-text">
        <h1>Personal Bird Sightings</h1>
        <div className="box">
          <input
            type="text"
            placeholder="Search by bird name..."
            value={searchQuery}
            onChange={handleSearch}
            style={{
              width: "300px",
              height: "40px",
              fontSize: "18px",
              marginBottom: "20px",
            }}
          />
        </div>
        <table className="table table-striped">
          <thead className="black-text">
            <tr>
              <th scope="col">Bird Sighted</th>
              <th scope="col">Date</th>
              <th scope="col">Notes</th>
              <th scope="col">Image</th>
              <th scope="col">Delete</th>
              <th scope="col">Edit</th>
            </tr>
          </thead>
          <tbody>
            {sortedSightings?.map((bird) => (
              <tr key={bird.id}>
                <td style={{ color: "black" }}>{bird.name}</td>
                <td style={{ color: "black" }}>{bird.date2}</td>
                <td style={{ color: "black" }}>{bird.notes}</td>
                <td>
                  <img
                    src={bird.picture_url}
                    alt="A friendly bird"
                    height="150"
                  />
                </td>
                <td>
                  <button
                    onClick={handleDelete}
                    value={bird.id}
                    type="button"
                    className="btn btn-danger"
                  >
                    Delete?
                  </button>
                </td>
                <td>
                  <button
                    onClick={handleEdit}
                    value={bird.id}
                    type="button"
                    className="btn btn-info"
                  >
                    Edit?
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
}
