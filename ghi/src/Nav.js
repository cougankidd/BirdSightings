import { NavLink } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useLogoutMutation } from "./redux/apiSlice";
import { useGetAccountQuery } from "./redux/apiSlice";
import "./css/Navbar.css";

function Nav() {
  const { data: account } = useGetAccountQuery();
  const navigate = useNavigate();
  const [logout] = useLogoutMutation();

  return (
    <nav className="navbar navbar-expand-lg navbar navbar-dark bg-dark padding 5em">
      <div className="container-fluid">
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className="collapse navbar-collapse ms-auto"
          id="navbarSupportedContent"
        >
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink to={"/"} className={"nav-link"}>
                Home
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to={"/birds"} className={"nav-link"}>
                All Birds
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to={"/leaderboard"} className={"nav-link"}>
                Leaderboard
              </NavLink>
            </li>
            {account && (
              <li className="nav-item">
                <NavLink to={"/postsighting"} className={"nav-link"}>
                  Post A Sighting
                </NavLink>
              </li>
            )}
            {account && (
              <li className="nav-item">
                <NavLink to={"/recentsightings"} className={"nav-link"}>
                  Recent Sightings
                </NavLink>
              </li>
            )}
            {account && (
              <li className="nav-item">
                <NavLink to={"/recentusersightings"} className={"nav-link"}>
                  Personal Sightings
                </NavLink>
              </li>
            )}
            {!account && (
              <li className="nav-item">
                <NavLink to={"/signup"} className={"nav-link"}>
                  Signup
                </NavLink>
              </li>
            )}
          </ul>
        </div>
        <div className="topnav-middle">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <h3 className="nav-item pink-text">Bitterroot Birds</h3>
          </ul>
        </div>
        <div className="topnav-right">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            {!account && (
              <li className="nav-item">
                <NavLink to={"/login"} className="btn btn-outline-success">
                  Login
                </NavLink>
              </li>
            )}
            {account && (
              <li className="nav-item">
                <button
                  onClick={() => {
                    logout();
                    navigate("/");
                  }}
                  type="button"
                  className="btn btn-outline-danger"
                >
                  {" "}
                  Logout
                </button>
              </li>
            )}
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
