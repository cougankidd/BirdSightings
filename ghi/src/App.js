import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./css/App.css";
import Nav from "./Nav";
import MainPage from "./MainPage";
import Signup from "./Accounts/Signup";
import Birds from "./Birds/birds";
import Leaderboard from "./Birds/Leaderboard";
import Login from "./Accounts/Login";
import PostSighting from "./Birds/PostSighting";
import Recentsightings from "./Birds/Recentsightings";
import RecentUserSightings from "./Birds/RecentUserSightings";
import EditSighting from "./Birds/EditSighting";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");
  return (
    <BrowserRouter basename={basename}>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/postsighting" element={<PostSighting />} />
          <Route path="/birds" element={<Birds />} />
          <Route path="/signup" element={<Signup />} />
          <Route path="/leaderboard" element={<Leaderboard />} />
          <Route path="/login" element={<Login />} />
          <Route path="/recentsightings" element={<Recentsightings />} />
          <Route path="/editsighting/:bird_id" element={<EditSighting />} />
          <Route
            path="/recentusersightings"
            element={<RecentUserSightings />}
          />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
