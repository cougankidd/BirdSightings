# main.py
from fastapi import FastAPI
from authenticator import authenticator
from routers import (
    accounts,
    bird_sightings,
    featured_bird,
    birds,
)
from fastapi.middleware.cors import CORSMiddleware
import os

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:3000")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def home():
    return True


app.include_router(authenticator.router, tags=["Accounts"])
app.include_router(accounts.router)
app.include_router(bird_sightings.router)
app.include_router(featured_bird.router)
app.include_router(birds.router)
