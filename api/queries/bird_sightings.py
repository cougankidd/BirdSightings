from pydantic import BaseModel
from datetime import date
from psycopg_pool import ConnectionPool
from typing import List
import os
from picture import get_photo

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class bird_sightingsIn(BaseModel):
    name: str
    date: date
    notes: str


class bird_sightingsOut(BaseModel):
    id: int
    name: str
    date: date
    notes: str
    picture_url: str
    user_id: int


class bird_sightingsOutwithdatestr(BaseModel):
    id: int
    name: str
    date: date
    date2: str
    notes: str
    picture_url: str
    user_id: int


class list_sightings(BaseModel):
    bird_sightings: List[bird_sightingsOut]


class list_sightings2(BaseModel):
    bird_sightings: List[bird_sightingsOutwithdatestr]


class single_bird_sighting(BaseModel):
    bird_sighting: bird_sightingsOut


class bird_sightingQueries:
    def create_sighting(self, bird: bird_sightingsIn, user_id: int):
        picture = get_photo(bird.name)
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    INSERT INTO bird_sightings
                        (name, date, user_id, picture_url, notes)
                    VALUES (%s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        bird.name,
                        bird.date,
                        user_id,
                        picture,
                        bird.notes,
                    ],
                )
                id = db.fetchone()[0]
                db.execute(
                    """
                    UPDATE accounts
                    SET bird_sightings = bird_sightings + 1
                    WHERE id = %s
                    """,
                    [user_id],
                )
                return bird_sightingsOut(
                    id=id,
                    name=bird.name,
                    date=bird.date,
                    notes=bird.notes,
                    user_id=user_id,
                    picture_url=picture,
                )

    def get_all_sightings(self) -> bird_sightingsOutwithdatestr:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id
                    , name
                    , date
                    , picture_url
                    , notes
                    , user_id
                    FROM bird_sightings
                    """
                )
                results = []
                for row in db.fetchall():
                    sighting = {}
                    for i, col in enumerate(db.description):
                        sighting[col.name] = row[i]
                    results.append(sighting)
            for name in results:
                name["date2"] = name["date"].strftime("%B, %d %Y")
            return {"bird_sightings": results}

    def get_user_sightings(self, user_id: int) -> bird_sightingsOutwithdatestr:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id
                    , name
                    , date
                    , picture_url
                    , notes
                    , user_id
                    FROM bird_sightings
                    WHERE user_id = %s
                    """,
                    [user_id],
                )
                results = []
                for row in db.fetchall():
                    sighting = {}
                    for i, col in enumerate(db.description):
                        sighting[col.name] = row[i]
                    results.append(sighting)
            for name in results:
                name["date2"] = name["date"].strftime("%B, %d %Y")
            return {"bird_sightings": results}

    def get_one_sighting(self, bird_sighting_id: int) -> single_bird_sighting:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id
                    , name
                    , date
                    , notes
                    , picture_url
                    , user_id
                    FROM bird_sightings
                    WHERE id = %s
                    """,
                    [bird_sighting_id],
                )
                row = db.fetchone()
                if row is None:
                    return {"error": "Sighting not found"}
                bird_sighting = {}
                for i, col in enumerate(db.description):
                    bird_sighting[col.name] = row[i]
                return {"bird_sighting": bird_sighting}

    def delete_sighting(self, bird_sighting_id: int, user_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id
                    , name
                    , date
                    , picture_url
                    , notes
                    FROM bird_sightings
                    WHERE id = %s AND user_id =%s
                    """,
                    (bird_sighting_id, user_id),
                )
                row = db.fetchone()
                if row is None:
                    return False
                else:
                    db.execute(
                        """
                        DELETE FROM bird_sightings
                        WHERE id = %s AND user_id =%s
                        RETURNING name
                        """,
                        (bird_sighting_id, user_id),
                    )
                    return True

    def edit_sighting(
        self, bird_sighting_id: int, bird: bird_sightingsIn, user_id: int
    ) -> bird_sightingsOut:
        picture = get_photo(bird.name)
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE bird_sightings
                    SET name = %s, date = %s, notes = %s, picture_url = %s
                    WHERE id = %s AND user_id = %s;
                    """,
                    [
                        bird.name,
                        bird.date,
                        bird.notes,
                        picture,
                        bird_sighting_id,
                        user_id,
                    ],
                )
                db.execute(
                    """
                    SELECT id
                    , name
                    , date
                    , picture_url
                    , notes
                    FROM bird_sightings
                    WHERE id = %s AND user_id = %s;
                    """,
                    (bird_sighting_id, user_id),
                )
                row = db.fetchone()
                bird_sighting = {}
                for i, col in enumerate(db.description):
                    bird_sighting[col.name] = row[i]

                return {"bird_sighting": bird_sighting}
