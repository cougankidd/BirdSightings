from pydantic import BaseModel
from psycopg_pool import ConnectionPool
import os
from typing import List

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class birdsOut(BaseModel):
    id: int
    bird_name: str
    picture_url: str


class birdsList(BaseModel):
    Birds: List[birdsOut]


class birdsQueries:
    def get_all_birds(self) -> birdsOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id,
                    bird_name,
                    picture_url
                    FROM birds
                    """
                )
                results = []
                for row in db.fetchall():
                    birds = {}
                    for i, col in enumerate(db.description):
                        birds[col.name] = row[i]
                    results.append(birds)
                return {"Birds": results}
