from pydantic import BaseModel
from psycopg_pool import ConnectionPool
import os
from typing import List
from psycopg.errors import UniqueViolation

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    username: str
    password: str


class AccountOut(BaseModel):
    id: str
    username: str
    bird_sightings: int


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountList(BaseModel):
    accounts: List[AccountOut]


class AccountQueries:
    def get(self, username: str) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, username, hashed_password, bird_sightings
                    FROM accounts
                    WHERE username=%s
                    """,
                    [username],
                )
                row = db.fetchone()
                account = {}
                for i, column in enumerate(db.description):
                    account[column.name] = row[i]
                return AccountOutWithPassword(**account)

    def create(
        self, info: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        account = info.dict()
        account[hashed_password] = hashed_password
        with pool.connection() as conn:
            with conn.cursor() as db:
                try:
                    db.execute(
                        """
                        INSERT INTO accounts
                            (username, hashed_password)
                        VALUES (%s, %s)
                        RETURNING id;
                        """,
                        [info.username, hashed_password],
                    )
                    id = db.fetchone()[0]
                    return AccountOutWithPassword(
                        id=id,
                        hashed_password=hashed_password,
                        username=info.username,
                        bird_sightings=0,
                    )
                except UniqueViolation:
                    raise DuplicateAccountError

    def get_all_users(self) -> AccountOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, username, bird_sightings
                    FROM accounts
                    """
                )
                results = []
                for row in db.fetchall():
                    account = {}
                    for i, col in enumerate(db.description):
                        account[col.name] = row[i]
                    results.append(account)
            return {"accounts": results}
