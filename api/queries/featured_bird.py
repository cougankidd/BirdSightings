from pydantic import BaseModel
from psycopg_pool import ConnectionPool
import os
from typing import List

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class featured_birdOut(BaseModel):
    id: int
    name: str
    description: str
    picture_url: str


class featured_birdList(BaseModel):
    Birds: List[featured_birdOut]


class featured_birdQueries:
    def get_all_featured_birds(self) -> featured_birdOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id,
                    name,
                    description,
                    picture_url
                    FROM featured_bird
                    """
                )
                results = []
                for row in db.fetchall():
                    birds = {}
                    for i, col in enumerate(db.description):
                        birds[col.name] = row[i]
                    results.append(birds)
                return {"Birds": results}
