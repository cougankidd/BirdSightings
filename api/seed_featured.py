import os
from psycopg_pool import ConnectionPool


pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])

birds = [
    {
        "name": "Harlequin Duck",
        "description": "A fairly small diving duck known as the Harlequin duck. It has bold patterns and lives in environments with fast-moving water and jagged rocks, which can be perceived as dangerous.", # noqa
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/63898051/900", # noqa
    },
    {
        "name": "Bald Eagle",
        "description": "Scavenges and hunts near bodies of water. Soars with wings flat, like a large, dark plank. Head appears large in flight; projects far in front of wings. Surprisingly weak-sounding vocalization is a series of high-pitched whistles.", # noqa
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306062281/900", # noqa
    },
    {
        "name": "White-tailed Ptarmigan",
        "description": "Plump, chickenlike bird found in tundra with a mix of rocks and mossy ground cover. Plumage changes throughout the year. In winter, both sexes are pure white. ", # noqa
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/295452811/900", # noqa
    },
    {
        "name": "Lapland Longspur",
        "description": "Sparrowlike ground-dweller; crouches low to the ground and walks with pattering footsteps. To separate from other longspurs, look for combination of face pattern, rufous nape, and rufous wing panel with white tips.", # noqa
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/171432921/900", # noqa
    },
    {
        "name": "Western Meadowlark",
        "description": "Streaked brown above and yellow below with distinctive black 'V' on breast. In flight, short wings and spiky tail with white outer feathers are apparent. Breeds in fields and grasslands throughout much of western North America, regularly as far east as Michigan.", # noqa
        "picture_url": "https://cdn.download.ams.birds.cornell.edu/api/v1/asset/306332801/900", # noqa
    }

]


def create_featured_bird(bird):
    with pool.connection() as conn:
        with conn.cursor() as db:
            db.execute(
                """
                INSERT INTO featured_bird
                    (name,
                    description,
                    picture_url)
                VALUES
                    (%s, %s, %s)
                RETURNING id;
                """,
                [
                    bird["name"],
                    bird["description"],
                    bird["picture_url"],
                ],
            )
    return True


def get_all_featured_birds():
    with pool.connection() as conn:
        with conn.cursor() as db:
            db.execute(
                """
                SELECT name
                FROM featured_bird
                """
            )
            results = []
            for row in db.fetchall():
                birds = {}
                for i, col in enumerate(db.description):
                    birds = row[i]
                results.append(birds)
            return results


all_birds = get_all_featured_birds()

for bird in birds:
    if bird["name"] not in all_birds:
        response = create_featured_bird(bird)
