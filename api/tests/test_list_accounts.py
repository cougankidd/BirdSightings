from fastapi.testclient import TestClient
from main import app
from queries.accounts import AccountQueries

# written by Prince

client = TestClient(app)


class FakeAccountQueries:
    def get_all_users(self):
        return {"accounts": []}


def test_list_users():
    # arrange
    app.dependency_overrides[AccountQueries] = FakeAccountQueries

    # act
    res = client.get("/api/accounts/")
    data = res.json()

    # assert
    assert res.status_code == 200
    assert data == {"accounts": []}

    app.dependency_overrides = {}
