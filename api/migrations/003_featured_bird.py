steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE featured_bird (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(100) NOT NULL UNIQUE,
            description VARCHAR(5000),
            picture_url VARCHAR(500) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE featured_bird;
        """,
    ],
]
