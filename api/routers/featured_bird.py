from fastapi import APIRouter, Depends
from queries.featured_bird import featured_birdQueries, featured_birdList


router = APIRouter()


@router.get(
    "/api/featured_bird",
    response_model=featured_birdList,
    tags=["Seeded_Data"],
)
def list_all_featured_birds(
    repo: featured_birdQueries = Depends(),
):
    return repo.get_all_featured_birds()
