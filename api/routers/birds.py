from fastapi import APIRouter, Depends
from queries.birds import birdsQueries, birdsList


router = APIRouter()


@router.get("/api/birds", response_model=birdsList, tags=["Seeded_Data"])
def list_all_birds(
    repo: birdsQueries = Depends(),
):
    return repo.get_all_birds()
