from fastapi import APIRouter, Depends
from queries.bird_sightings import (
    bird_sightingQueries,
    bird_sightingsIn,
    bird_sightingsOut,
    single_bird_sighting,
    list_sightings2,
)


from authenticator import authenticator

router = APIRouter()


@router.post(
    "/api/bird_sightings",
    response_model=bird_sightingsOut,
    tags=["Bird_Sightings"],
)
def create_bird_sighting(
    bird: bird_sightingsIn,
    account: dict = Depends(authenticator.get_current_account_data),
    repo: bird_sightingQueries = Depends(),
):
    return repo.create_sighting(bird, account["id"])


@router.get(
    "/api/bird_sightings",
    response_model=list_sightings2,
    tags=["Bird_Sightings"],
)
def list_all_sightings(
    repo: bird_sightingQueries = Depends(),
):
    return repo.get_all_sightings()


@router.get(
    "/api/single_bird_sighting/{bird_sighting_id}",
    response_model=single_bird_sighting,
    tags=["Bird_Sightings"],
)
def list_a_sighting(
    bird_sighting_id: int,
    repo: bird_sightingQueries = Depends(),
):
    return repo.get_one_sighting(bird_sighting_id)


@router.get(
    "/api/user_bird_sightings",
    response_model=list_sightings2,
    tags=["Bird_Sightings"],
)
def list_user_sightings(
    account: dict = Depends(authenticator.get_current_account_data),
    repo: bird_sightingQueries = Depends(),
):
    return repo.get_user_sightings(account["id"])


@router.delete(
    "/api/bird_sightings/{bird_sighting_id}",
    response_model=bool,
    tags=["Bird_Sightings"],
)
def delete_bird_sighting(
    bird_sighting_id: int,
    account: dict = Depends(authenticator.get_current_account_data),
    repo: bird_sightingQueries = Depends(),
):
    return repo.delete_sighting(bird_sighting_id, account["id"])


@router.put(
    "/api/bird_sightings/{bird_sighting_id}",
    response_model=dict,
    tags=["Bird_Sightings"],
)
def edit_bird_sighting(
    bird_sighting_id: int,
    bird: bird_sightingsIn,
    account: dict = Depends(authenticator.get_current_account_data),
    repo: bird_sightingQueries = Depends(),
):
    return repo.edit_sighting(bird_sighting_id, bird, account["id"])
