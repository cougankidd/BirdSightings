## 6/26/23

Worked on forking project, making sure docker compose up is working and watching backendauth video

## 6/27/23

Worked on auth, got create and get working

## 6/28/23

Worked on queries, routes, added tables

## 6/29/23

Created several queries including bird_sightings, birds, featured_bird and accounts and there router files, created seed_birds.py and seed_featured.py to provide users with a dropdown menu choices

## 6/30/23

Did some additional touches on the tables for accounts, bird_sightings, featured_biord and birds, also discussed what to research during break

## 7/10/23

Added tags to the docs, also did some additional fixes on bird_sightings.py queries and added a history querie.
Added a picture_url field to bird sightings and used pexels api to use the name of the bird to get a image and save it to the field
Added a bird sighting count field to accounts and made it when a sighting is created the count is updated

## 7/11/23

Added redux to project, Also created a list page for all birds that shows cards with titles and images.
Added a leaderboard page with users listed by most bird sightings.

## 7/12/23

Assisted teammates on doing redux and setting up login and signup pages.

## 7/13/23

Assisted teammates on creating Post Sighting page, made some queries changes, adding logout feature to navbar and making some bug fixes.

## 7/17/23

Added Delete and user recent sighting api slice, created user recent sighting page and implemented delete feature for bird sightings
Added a get bird query for getting a single sighting and created a router for it.

## 7/18/23

Restyled login and signup page
Redid Navbar to move login/signup buttons to the right
Got the edit page fully working

## 7/19/23

Did code cleaning, reformatted datetime by adding a new data in queries and routers and changing the recentsighting.js and recentusersighting.js

## 7/20/23-7/26/23

Updated readme.md and studied websockets
