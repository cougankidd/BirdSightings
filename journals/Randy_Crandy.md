6/28
Worked on adding User History page to lists Users' submissions history


6/29
Added differentiating functionalities to user_history.
Also added methods to track how many unique sightings have been made vs how many remain, checked against our database of birds.
Also created methods and routes to create a record/history of sightings for a specific user; to retrieve all of a users records; and to retrieve a specific record from a users history


6/30
Potential elevenlabs integration stretch goal:

Create a new route for the audio conversion: Create a new route that'll handle the conversion of text to audio. This route will take the bird's name, either using websockets or an addition encyclopedia api, return text about the bird as input, send that text to the ElevenLabs API for conversion, and then return the audio file or a link to the audio file as a response

Integrate the ElevenLabs API: Use the ElevenLabs API to convert the text to audio. This will involve making a POST request to the ElevenLabs API with the text and the voice ID as the payload. Will need to handle the response from the ElevenLabs API, which will likely be a link to the audio file.

Play the audio file: When a user views a bird sighting post, we'll need to provide a way for them to play the audio file or for the audio file to autoplay in the user's browser (This could be a simple play button next to the text about the bird or by asynchronously loading audio with the webpage to cut down on latency. Audio could play as page loads- starting while user is being redirected to their user_history and playing as the users webpage loads pictures?). When the audio plays it should read the returned text in the voice of the Pokedex (Or Plan B: David Attenborough. If we can't find high enough quality audio to clone a voice from).
