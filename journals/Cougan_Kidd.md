6/26/2023
Created a group, forked the starting repo, and got everything up and running. Currently going over backend auth.

6/27/2023
Assisted Prince with auth. Reviewing fastapi videos.

6/28/2023
Worked on tables for bird sightings and featured bird. Created featured bird in both queries and routers.

6/29/2023
Finished birds and featured bird routers, table, and queries. Fixed bugs.

6/30/2023
reviewed code. minor touch ups. Planned material to review and what we will work on post break.

7/10/2023
Started working on frontend. created a basic mainpage. created a functional signup page that redirects to the mainpage when signup is good. Trying to figure out how to throw an error when a username that already exists is used. Finished populating seed bird data with all possible birds in the bitterroot valley.

7/11/2023
Finished populating seed bird data with all the picture urls. Fix bugs and cleaned up unsued code. modified homepage. assisted prince with leaderboard page fixing sorting issue so that it displays the users in order of most total sightings.

7/12/2023
Fixed errors in seed bird data. Added links to nav bar. Built out the signup with redux with assistance from prince. Assisted Marc with building the login page.

7/13/2023
Built PostSightings page so that a logged in user can report a bird, date, and any notes. minor fixes to page. Also set up logout functionality. added navigation features from pages when certain actions occur. assisted with recent sightings page.

7/14/2023
Added delete and update/edit functionality to the bird sightings.

7/17/2023
Fixed all current flake8 errors. Added a search bar functionality to the all birds and recent sightings pages allowing you to find a specific bird or just a type of bird. This feature will also be used in the user page to filter through their sightings.

7/18/2023
Added search functionality to recent user sightings page. Working on styling for the website. implemented unit tests.

7/19/2023
added error handling on the front end for bad requests with signup and login. Fixed background issue with all birds page! adjusted backgrounds for all pages so that it is fixed and elements displayed on the page will roll over it. Adjusted font colors for uniformity across the page.

7/28/2023
Cleaned up unused files and got rid of console logs.
